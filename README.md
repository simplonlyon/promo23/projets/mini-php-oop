# mini-php-oop
Projet à faire en 3 jours, on va donc partir d'avantage sur un genre que "gros exercice" plutôt que sur un projet libre comme les autres.

## Objectif
En utilisant les classes PHP, créer un système de réservation de place pour une salle de concert/de théatre/autre.

Les fonctionnalités attendues sont :
* Pouvoir créer plusieurs salles
* Chaque salle sera composée d'un ensemble de places qui auront des caractéristiques comme par exemple : un numéro, un emplacement, un prix
* Possibilité de créer une réservation pour une place pour une date et une heure donnée (avec vérification que la place n'est pas déjà prise au créneau indiqué)
* Dans les méthodes utilitaires de la salle on souhaite :
    * Savoir si la salle est complète à une heure donnée
    * En se basant sur les réservations, avoir le total des bénéfices de la salle sur un créneau/une journée/une durée spécifique (vous pouvez ne faire qu'un seul des trois)
    * Obtenir toutes les réservation d'une personne (et pourquoi pas sur un laps de temps donné, "passées", "futures", autre)
    * Bonus : avoir les statistiques de fréquentation par créneau

Dans les simplifications, on assigne ici directement un prix à la place, si vous êtes à fond, vous pouvez faire en sorte que le prix dépende de la place mais aussi du spectacle qu'on va voir, mais ça complexifie pas mal. Pareillement, pas de gestion de user attendue, au pire on peut juste mettre un nom pour la réservation.


## Réalisation
Pas d'interface graphique ou de base de données attendues pour ce projet, l'idée est de travailler les classes, donc vous créerez vos instances manuellement dans le point d'entrée et vous interagirez directement avec vos méthodes dans ce même point d'entrée pour voir si vous avez bien les résultats voulus pour les différents méthodes. (Après, si vous avez un truc fonctionnel avant la fin, vous pouvez faire en sorte de générer une petite interface graphique en HTML si vous le souhaitez mais ce n'est pas la priorité)

Vous pouvez commencer par réfléchir à la structure et aux classes que vous aurez en fonction des fonctionnalités demandées, et pourquoi pas de les modéliser sous forme d'un diagramme de classes avec les propriétés et méthodes, mais ce n'est pas obligatoire, faites le si ça vous aide.


Pour l'exercice, vous pouvez voir pour créer une base de données pour le projet, mais juste pour le moment il n'y aura pas d'interaction entre la base de données et les classe, ça reste néanmoins intéressant

